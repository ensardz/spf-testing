"use strict";

const build = require("@microsoft/sp-build-web");
const path = require("path");

build.addSuppression(
  `Warning - [sass] The local CSS class 'ms-Grid' is not camelCase and will not be type-safe.`
);

build.configureWebpack.mergeConfig({
  additionalConfiguration: (generatedConfiguration) => {
    if (!generatedConfiguration.resolve.alias) {
      generatedConfiguration.resolve.alias = {};
    }

    //References for generated sources

    generatedConfiguration.resolve.alias["@components"] = path.resolve(
      __dirname,
      "lib/webparts/helloWorld/components/"
    );

    generatedConfiguration.resolve.alias["@src"] = path.resolve(
      __dirname,
      "lib"
    );

    generatedConfiguration.resolve.alias["@api"] = path.resolve(
      __dirname,
      "lib/webparts/helloWorld/api/"
    );

    generatedConfiguration.resolve.alias["@interfaces"] = path.resolve(
      __dirname,
      "lib/webparts/helloWorld/core/interfaces/"
    );

    return generatedConfiguration;
  },
});

build.initialize(require("gulp"));
