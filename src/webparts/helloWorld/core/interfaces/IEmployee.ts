export interface IEmployee {
  name: string;
  portrait: string;
  location: string;
}
