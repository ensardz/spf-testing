import * as React from "react";
import { WebPartContext } from "@microsoft/sp-webpart-base";

const AppWebPartContext = React.createContext(null);

export const AppWebPartProvider: any = AppWebPartContext.Provider;
export const AppWebPartConsumer: any = AppWebPartContext.Consumer;
export interface AppWebPartProviderProps {
  value: WebPartContext;
}

export default AppWebPartContext;
