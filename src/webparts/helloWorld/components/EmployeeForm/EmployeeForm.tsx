import * as React from "react";
import { IEmployee } from "@interfaces/IEmployee";
import {
  DefaultButton,
  PrimaryButton,
  TextField,
  Stack,
} from "office-ui-fabric-react";

interface IEmployeeFormProps {
  action: string;
  employee?: IEmployee;
  onComplete(employee): any;
  onEdit?(emplotee): any;
  onDismiss(): any;
}

function EmployeeForm({
  action,
  employee,
  onComplete,
  onEdit,
  onDismiss,
}: IEmployeeFormProps) {
  const [name, setName] = React.useState("");
  const [location, setLocation] = React.useState("");
  const [portrait, setPortrait] = React.useState("");

  React.useEffect(() => {
    if (employee) {
      if (employee.name || employee.location || employee.portrait) {
        setName(employee.name);
        setLocation(employee.location);
        setPortrait(employee.portrait);
      }
    }
  }, []);

  const handleOnComplete = () => {
    if (name && location && portrait) {
      switch (action) {
        case "create":
          onComplete({ name, location, portrait });
          break;
        case "edit":
          onEdit({ name, location, portrait });
          break;
      }
      onDismiss();
    }
  };
  const handleNameChange = (event, value) => {
    event.preventDefault();
    setName(value);
  };
  const handleLocationChange = (event, value) => {
    event.preventDefault();
    setLocation(value);
  };
  const handlePortraitChange = (event, value) => {
    event.preventDefault();
    setPortrait(value);
  };

  return (
    <div style={{ padding: 16, paddingLeft: 32, paddingRight: 32 }}>
      <h1>{action === "create" ? "Crear Employee" : "Editar Employee"}</h1>
      <form>
        <TextField
          id="name"
          label="Name"
          value={name}
          onChange={handleNameChange}
        />
        <TextField
          id="location"
          label="Location"
          value={location}
          onChange={handleLocationChange}
        />
        <TextField
          id="portrait"
          label="Portrait"
          value={portrait}
          onChange={handlePortraitChange}
        />
        <Stack horizontal style={{ paddingTop: 16 }}>
          <DefaultButton text="Cancel" onClick={onDismiss} />
          <PrimaryButton text="Add" onClick={handleOnComplete} />
        </Stack>
      </form>
    </div>
  );
}
export default EmployeeForm;
