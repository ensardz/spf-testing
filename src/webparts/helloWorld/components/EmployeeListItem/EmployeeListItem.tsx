import * as React from "react";
import { IEmployee } from "@interfaces/IEmployee";
import { IEmployeeListItemProps } from "./IEmployeeListItemProps";
import { IconButton, Stack } from "office-ui-fabric-react";

function EmployeeListItem({
  name,
  portrait,
  location,
}: IEmployeeListItemProps) {
  return (
    <div className="m-row item">
      <img className="m-col" alt={name} src={portrait} />
      <div className="m-col">
        <p>{name}</p>
        <p>{location}</p>
        <Stack horizontal horizontalAlign="end">
          <IconButton iconProps={{ iconName: "Edit" }} />
          <IconButton iconProps={{ iconName: "Delete" }} />
        </Stack>
      </div>
    </div>
  );
}

export default EmployeeListItem;
