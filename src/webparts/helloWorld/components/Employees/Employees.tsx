import * as React from "react";
import styles from "./Employee.module.scss";

import { getEmployeeList, addEmployee } from "@api/index";
import { WebPartContext } from "@microsoft/sp-webpart-base";
import { Modal, PrimaryButton } from "office-ui-fabric-react";

import AppWebPartContext from "@src/webparts/helloWorld/WebPartContext";
import EmployeesList from "@components/EmployeesList/EployeesList";
import EmployeeForm from "@components/EmployeeForm/EmployeeForm";
import { IEmployee } from "@interfaces/IEmployee";

function Employees() {
  const context: WebPartContext = React.useContext(AppWebPartContext);

  const [employeeList, setEmployeeList] = React.useState([]);
  const [isModalOpen, setIsModalOpen] = React.useState(false);

  const fetchEmployeeList = async () => {
    const response: Array<IEmployee> = await getEmployeeList();
    setEmployeeList(response);
  };

  React.useEffect(() => {
    fetchEmployeeList();
  }, []);

  const handleEmployeeComplete = async (employee: IEmployee) => {
    let response = await addEmployee(employee);
    if (response) {
      fetchEmployeeList();
    }
  };

  const onDismissModal = () => setIsModalOpen(!isModalOpen);

  return (
    <div className={styles.employeeContainer}>
      <p className="title">Employees</p>
      <EmployeesList employeeList={employeeList} />
      <PrimaryButton
        text="Create employee"
        onClick={() => {
          setIsModalOpen(true);
        }}
      />
      <Modal isOpen={isModalOpen} onDismiss={onDismissModal}>
        <EmployeeForm
          action="create"
          onComplete={handleEmployeeComplete}
          onDismiss={onDismissModal}
        />
      </Modal>
    </div>
  );
}

export default Employees;
