import * as React from "react";
import styles from "./EmployeeList.module.scss";
import EmployeeListItem from "@components/EmployeeListItem/EmployeeListItem";

function EmployeeList({ employeeList }) {
  return (
    <div className={styles.employeeList}>
      {employeeList.map(({ name, portrait, location }, index) => {
        return (
          <EmployeeListItem
            name={name}
            portrait={portrait}
            location={location}
            key={index + name}
          />
        );
      })}
    </div>
  );
}

export default EmployeeList;
