import { SPHttpClient, SPHttpClientResponse } from "@microsoft/sp-http";
import { Environment, EnvironmentType } from "@microsoft/sp-core-library";
import { WebPartContext } from "@microsoft/sp-webpart-base";

// SP
import { sp } from "@pnp/sp";
import "@pnp/sp/webs";
import "@pnp/sp/lists";
import "@pnp/sp/items";

const mockEmployeeList = [
  {
    name: "Enrique Salas Rodriguez",
    location: "Monterrey",
    portrait: "https://randomuser.me/api/portraits/men/1.jpg",
  },
  {
    name: "Max Tenorio Contoso",
    location: "Guadalupe",
    portrait: "https://randomuser.me/api/portraits/men/2.jpg",
  },
];

const isLocal = Environment.type === EnvironmentType.Local;

export async function getEmployeeList() {
  if (isLocal) return mockEmployeeList;
  const response: [] = await sp.web.lists.getByTitle("Employees").items.get();

  return response.map((employee: any) => {
    return { ...employee, name: employee.Title };
  });
}
export async function addEmployee({ name, location, portrait }) {
  if (isLocal) {
    mockEmployeeList.push({ name, location, portrait });
    return true;
  }
  const response: any = await sp.web.lists
    .getByTitle("Employees")
    .items.add({ Title: name, location, portrait });
  if (response.data) {
    return true;
  }
  return false;
}
